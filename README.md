# Source Distribution for GHC with interactive frontend support

This repository hosts source distribution tarballs for GHC compiler from this
experimental branch:

	https://gitlab.haskell.org/complyue/ghc/tree/ghc-8.6-ife
